/**
 * Copyright (C) 2022 yogiastawan (yogi.astawan@gmail.com)
 *
 * This file is part of Ether Matrix OglES Lib.
 *
 * Ether Matrix OglES Lib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Ether Matrix OglES Lib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Ether Matrix OglES Lib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ether_matrix_ogles.h>
#include <math.h>

void ether_vec3_add(e_vec3 result, e_vec3 first, e_vec3 second)
{
    char i = 0;
    for (i = 0; i < 3; i++)
    {
        result[i] = first[i] + second[i];
    }
}
void ether_vec3_sub(e_vec3 result, e_vec3 first, e_vec3 second)
{
    char i = 0;
    for (i = 0; i < 3; i++)
    {
        result[i] = first[i] - second[i];
    }
}
float ether_vec3_dot_product(e_vec3 first, e_vec3 second)
{
    float res = 0;
    char i = 0;
    for (i = 0; i < 3; i++)
    {
        res += first[i] * second[i];
    }
    return res;
}
void ether_vec3_cross_product(e_vec3 result, e_vec3 first, e_vec3 second)
{
    result[0] = (first[1] * second[2]) - (first[2] * second[1]);
    result[1] = (first[2] * second[0]) - (first[0] * second[2]);
    result[2] = (first[0] * second[1]) - (first[1] * second[0]);
}
float ether_vec3_length(e_vec3 vect)
{
    return sqrtf(ether_vec3_dot_product(vect, vect));
}

void ether_vec3_scale(e_vec3 result, e_vec3 vect, float sK)
{
    char i = 0;
    for (i = 0; i < 3; i++)
    {
        result[i] = vect[i] * sK;
    }
}

void ether_vec3_translate(e_vec3 result, e_vec3 vect, float sT)
{
    char i = 0;
    for (i = 0; i < 3; i++)
    {
        result[i] = vect[i] + sT;
    }
}

void ether_vec3_rotate_around_x(e_vec3 result, e_vec3 vect, float rad)
{
    result[0] = vect[0];
    result[1] = (vect[1] * cosf(rad)) - (vect[2] * sinf(rad));
    result[2] = (vect[1] * sinf(rad)) + (vect[2] * cosf(rad));
}
void ether_vec3_rotate_around_y(e_vec3 result, e_vec3 vect, float rad)
{
    result[0] = (vect[0] * cosf(rad)) + (vect[2] * sinf(rad));
    result[1] = vect[1];
    result[2] = (vect[2] * cosf(rad)) - (vect[0] * sinf(rad));
}
void ether_vec3_rotate_around_z(e_vec3 result, e_vec3 vect, float rad)
{
    result[0] = (vect[0] * cosf(rad)) - (vect[1] * sinf(rad));
    result[1] = (vect[0] * sinf(rad)) + (vect[1] * cosf(rad));
    result[2] = vect[2];
}

void ether_vec3_normalize(e_vec3 result, e_vec3 vect)
{
    float k = 1 / ether_vec3_length(vect);
    ether_vec3_scale(result, vect, k);
}

void ether_vec4_add(e_vec4 result, e_vec4 first, e_vec4 second)
{
}
void ether_vec4_sub(e_vec4 result, e_vec4 first, e_vec4 second)
{
}
float ether_vec4_dot_product(e_vec4 first, e_vec4 second)
{
}
void ether_vec4_cross_product(e_vec4 result, e_vec4 first, e_vec4 second)
{
}
float ether_vec4_length(e_vec4 first, e_vec4 second)
{
}
void ether_vec4_normalize(e_vec4 result, e_vec4 vect)
{
}