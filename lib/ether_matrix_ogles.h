// Copyright (C) 2022 yogiastawan (yogi.astawan@gmail.com)
//
// This file is part of Ether Matrix OglES Lib.
//
// Ether Matrix OglES Lib is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Ether Matrix OglES Lib is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Ether Matrix OglES Lib.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ETHER_MATRIX_OGLES_H__
#define __ETHER_MATRIX_OGLES_H__

#ifdef __cplusplus
extern c
{
#endif

    typedef float e_vec3[3];
    typedef float e_vec4[4];

    void ether_vec3_add(e_vec3 result, e_vec3 first, e_vec3 second);
    void ether_vec3_sub(e_vec3 result, e_vec3 first, e_vec3 second);
    float ether_vec3_dot_product(e_vec3 first, e_vec3 second);
    void ether_vec3_cross_product(e_vec3 result, e_vec3 first, e_vec3 second);
    float ether_vec3_length(e_vec3 vect);
    void ether_vec3_scale(e_vec3 result, e_vec3 vect, float sK);
    void ether_vec3_translate(e_vec3 result, e_vec3 vect, float sT);
    void ether_vec3_rotate_around_x(e_vec3 result, e_vec3 vect, float rad);
    void ether_vec3_rotate_around_y(e_vec3 result, e_vec3 vect, float rad);
    void ether_vec3_rotate_around_z(e_vec3 result, e_vec3 vect, float rad);
    void ether_vec3_normalize(e_vec3 result, e_vec3 vect);

    void ether_vec4_add(e_vec4 result, e_vec4 first, e_vec4 second);
    void ether_vec4_sub(e_vec4 result, e_vec4 first, e_vec4 second);
    float ether_vec4_dot_product(e_vec4 first, e_vec4 second);
    void ether_vec4_cross_product(e_vec4 result, e_vec4 first, e_vec4 second);
    float ether_vec4_length(e_vec4 first, e_vec4 second);
    void ether_vec4_normalize(e_vec4 result, e_vec4 vect);

#ifdef __cplusplus
}
#endif

#endif /*__ETHER_MATRIX_OGLES_H__*/