/**
 * Copyright (C) 2022 yogiastawan (yogi.astawan@gmail.com)
 * 
 * This file is part of Ether Matrix OglES Lib.
 * 
 * Ether Matrix OglES Lib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Ether Matrix OglES Lib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Ether Matrix OglES Lib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include<ether_matrix_ogles.h>

int main(int argc, char const *argv[])
{
    e_vec3 a={3,5,7};
    e_vec3 b={2,4,6};
    return 0;
}
